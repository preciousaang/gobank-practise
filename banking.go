package main

import (
	"fmt"

	"example.com/banking/fileops"
	"github.com/Pallinder/go-randomdata"
)

const accountBalanceFile = "balance.txt"

func main() {
	fmt.Println("Welcome to GoBank!")
	fmt.Println("Reach use 24/7", randomdata.PhoneNumber())
	accountBalance, error := fileops.GetFloatFromFile(accountBalanceFile)
	if error != nil {
		panic(error)
	}
	for {
		presentOptions()

		var choice int

		fmt.Scan(&choice)

		switch choice {
		case 1:
			fmt.Println("You balance is:", accountBalance)

		case 2:
			fmt.Print("Enter amount: ")
			var depositAmount float64
			fmt.Scan(&depositAmount)
			if depositAmount <= 0 {
				fmt.Println("Invalid amount entered. Must be greater than 0")
				return
			}
			accountBalance += depositAmount
			fmt.Println("Balanced updated! New amount:", accountBalance)
			fileops.WriteFloatToFile(accountBalance, accountBalanceFile)

		case 3:
			fmt.Print("Enter amount: ")
			var withdrawalAmount float64
			fmt.Scan(&withdrawalAmount)
			if withdrawalAmount > accountBalance {
				fmt.Printf("Invalid amount entered. Must be less or equal to the account balance (%v)\n", accountBalance)
				return
			}
			if withdrawalAmount <= 0 {
				fmt.Println("Invalid amount entered. Must be greater than 0")
				return
			}
			accountBalance -= withdrawalAmount
			fmt.Println("Withdrawal successful. New balance:", accountBalance)
			fileops.WriteFloatToFile(accountBalance, accountBalanceFile)

		case 4:
			fmt.Println("Goodbye!")
			fmt.Println("Thanks for using our bank!")
			return
		default:
			fmt.Println("Invalid selection")
		}

		for i := 1; i <= 2; i++ {
			println("==========")
		}

	}

}
