package fileops

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

func WriteFloatToFile(value float64, fileName string) {
	valueText := fmt.Sprint(value)
	os.WriteFile(fileName, []byte(valueText), 0644)
}

func GetFloatFromFile(fileName string) (float64, error) {
	data, error := os.ReadFile(fileName)
	if error != nil {
		errorMessage := fmt.Sprintf("file %s does not exist", fileName)
		return 100, errors.New(errorMessage)
	}
	balanceText := string(data)
	balance, error := strconv.ParseFloat(balanceText, 64)
	if error != nil {
		return 100, errors.New("there was an error parsing the text")
	}
	return balance, nil
}
